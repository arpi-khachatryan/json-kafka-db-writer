# JSON-Kafka-DB-Writer

JSON-Kafka-DB-Writer is a Spring Boot project designed to manage financial data effectively. It receives JSON messages, sends them through Kafka, saves them in a MySQL database, and logs relevant information for monitoring and debugging purposes.

## Features

- **Kafka Integration**: Sends messages through [Kafka](https://kafka.apache.org/) for others to use.
- **MySQL Database**: Stores processed data for long-term storage using [MySQL](https://www.mysql.com/).
- **Logging**: Logs relevant information for monitoring and debugging.

## Usage

- Send JSON Messages: Input financial data in JSON format.
- Processing and Forwarding: The application processes and sends data through Kafka.
- Database Storage: Concurrently, it stores data in a MySQL database for future use.
- Logging: Relevant information is logged for monitoring and debugging purposes.
