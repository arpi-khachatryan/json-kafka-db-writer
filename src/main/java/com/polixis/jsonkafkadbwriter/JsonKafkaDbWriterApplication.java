package com.polixis.jsonkafkadbwriter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */


@SpringBootApplication
public class JsonKafkaDbWriterApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsonKafkaDbWriterApplication.class, args);
    }
}