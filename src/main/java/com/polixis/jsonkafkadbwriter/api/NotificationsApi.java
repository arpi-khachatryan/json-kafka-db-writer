package com.polixis.jsonkafkadbwriter.api;

import com.polixis.jsonkafkadbwriter.dto.NotificationRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

public interface NotificationsApi {

    @Operation(
            summary = "Publish a new finance-related notification.",
            description = "Publishes a new notification message related to finance to subscribed users."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Notification published successfully.",
                            content = @Content(
                                    schema = @Schema(implementation = NotificationRequest.class),
                                    mediaType = APPLICATION_JSON_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Failed to publish notification. An error occurred while processing the request.",
                            content = @Content(mediaType = APPLICATION_JSON_VALUE)
                    )
            }
    )
    ResponseEntity<?> publish(NotificationRequest notificationRequest, BindingResult bindingResult);
}