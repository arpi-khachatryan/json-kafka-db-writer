package com.polixis.jsonkafkadbwriter.api.controller;

import com.polixis.jsonkafkadbwriter.api.NotificationsApi;
import com.polixis.jsonkafkadbwriter.dto.NotificationRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/notifications")
public class NotificationsController implements NotificationsApi {

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    @PostMapping
    public ResponseEntity<String> publish(@Valid @RequestBody NotificationRequest notificationRequest, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            log.error("Failed to bind notification request data: {}", bindingResult.getAllErrors());
            return ResponseEntity.badRequest().body("Please provide valid notification data.");
        }
        log.info("Received notification: {}", notificationRequest);
        try {
            kafkaTemplate.send("jsonKafkaDBWriter", notificationRequest.notification());
            log.info("Notification published successfully.");
            return ResponseEntity.ok().body("Notification published successfully.");
        } catch (Exception e) {
            log.error("Failed to publish notification: {}", e.getMessage(), e);
            return ResponseEntity.internalServerError().body("Failed to publish notification. An error occurred while processing the request.");
        }
    }
}