package com.polixis.jsonkafkadbwriter.config;

import com.polixis.jsonkafkadbwriter.errorHandler.CustomKafkaListenerErrorHandler;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * Author: Arpi Khachatryan
 * Date: March 3, 2024
 */

@Configuration
@AllArgsConstructor
public class ProjectConfig {

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Bean
    public CustomKafkaListenerErrorHandler customKafkaListenerErrorHandler() {
        return new CustomKafkaListenerErrorHandler(kafkaTemplate);
    }
}