package com.polixis.jsonkafkadbwriter.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

@Slf4j
@Configuration
public class TopicConfiguration {

    @Bean
    public NewTopic jsonKafkaDBWriterTopic() {
        log.info("Creating Kafka Topic 'jsonKafkaDBWriter'.");
        return TopicBuilder.name("jsonKafkaDBWriter")
                .build();
    }

    @Bean
    public NewTopic errorTopic() {
        log.info("Creating Kafka Topic 'notificationsPublishingErrorTopic'.");
        return TopicBuilder.name("notificationsPublishingErrorTopic")
                .build();
    }
}