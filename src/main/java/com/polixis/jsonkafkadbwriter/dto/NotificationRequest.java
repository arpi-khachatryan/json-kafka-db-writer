package com.polixis.jsonkafkadbwriter.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

public record NotificationRequest(@NotNull @Size(min = 1, max = 100) String notification) {
}