package com.polixis.jsonkafkadbwriter.errorHandler;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.kafka.listener.ListenerExecutionFailedException;
import org.springframework.messaging.Message;

/**
 * Author: Arpi Khachatryan
 * Date: March 3, 2024
 */

@Slf4j
@AllArgsConstructor
public class CustomKafkaListenerErrorHandler implements KafkaListenerErrorHandler {

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public Object handleError(Message<?> message, ListenerExecutionFailedException exception) {
        log.info("Notification data received: {}.", message);
        log.info("Creating an error response object");

        String exceptionName = exception.getClass().getSimpleName();
        String errorMsg = "Error occurred during message processing. Error: " + exceptionName;

        log.info("Publishing the error response to the Kafka topic.");
        kafkaTemplate.send("notificationsPublishingErrorTopic", errorMsg);
        return errorMsg;
    }
}