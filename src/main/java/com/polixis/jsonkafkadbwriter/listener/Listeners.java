package com.polixis.jsonkafkadbwriter.listener;

import com.polixis.jsonkafkadbwriter.service.NotificationsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

@Slf4j
@Component
public class Listeners {

    private final NotificationsService notificationsService;

    public Listeners(NotificationsService notificationsService) {
        this.notificationsService = notificationsService;
    }

    @KafkaListener(topics = "jsonKafkaDBWriter", groupId = "NotificationProcessor", errorHandler = "customKafkaListenerErrorHandler")
    public void listener(String data) {
        log.info("Listener received message: {}.", data);
        log.info("Saving the received data: {} to the database.", data);
        notificationsService.processNotification(data);
        log.info("Data saved successfully.");
    }

    @KafkaListener(topics = "notificationsPublishingErrorTopic", groupId = "NotificationProcessorError")
    public void processErrorMessages(String errorResponse) {
        log.error("Received error response: {}", errorResponse);
    }
}