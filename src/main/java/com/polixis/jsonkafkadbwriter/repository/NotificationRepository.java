package com.polixis.jsonkafkadbwriter.repository;

import com.polixis.jsonkafkadbwriter.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

public interface NotificationRepository extends JpaRepository<Notification, Long> {
}