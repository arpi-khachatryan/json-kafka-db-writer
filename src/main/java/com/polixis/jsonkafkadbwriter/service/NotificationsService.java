package com.polixis.jsonkafkadbwriter.service;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

public interface NotificationsService {
    void processNotification(String data);
}