package com.polixis.jsonkafkadbwriter.service.serviceImpl;

import com.polixis.jsonkafkadbwriter.model.Notification;
import com.polixis.jsonkafkadbwriter.repository.NotificationRepository;
import com.polixis.jsonkafkadbwriter.service.NotificationsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Author: Arpi Khachatryan
 * Date: March 2, 2024
 */

@Slf4j
@Service
@AllArgsConstructor
public class NotificationsServiceImpl implements NotificationsService {

    private final NotificationRepository repository;

    @Override
    public void processNotification(String data) {
        log.info("Process the received data: {}.", data);
        Notification notification = new Notification();
        notification.setMessage(data);
        repository.save(notification);
        log.info("Data saved successfully.");
    }
}